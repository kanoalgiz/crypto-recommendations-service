package com.epam.recommendationservice.converters;
import com.epam.recommendationservice.domain.CurrencyStatistics;
import com.epam.recommendationservice.dto.CurrencyStatisticsView;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CurrencyStatisticsToCurrencyStatisticsView implements Converter<CurrencyStatistics, CurrencyStatisticsView> {

  @Override
  public CurrencyStatisticsView convert(CurrencyStatistics source) {
    return CurrencyStatisticsView.builder()
        .currencySymbol(source.getSymbol())
        .startDate(LocalDate.ofInstant(Instant.ofEpochMilli(source.getOldestRecord().getTimestamp()), ZoneId.of("UTC")))
        .endDate(LocalDate.ofInstant(Instant.ofEpochMilli(source.getNewestRecord().getTimestamp()), ZoneId.of("UTC")))
        .maxPrice(source.getMostExpensiveRecord().getPrice())
        .minPrice(source.getCheapestRecord().getPrice())
        .newestPrice(source.getNewestRecord().getPrice())
        .oldestPrice(source.getOldestRecord().getPrice())
        .normalizedRange(source.getNormalizedRange())
        .build();
  }
}
