package com.epam.recommendationservice.exceptions;

public class StatisticsRequestException extends RuntimeException {

  public StatisticsRequestException(String message) {
    super(message);
  }
}
