package com.epam.recommendationservice.exceptions;

import static org.springframework.http.HttpStatus.*;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

  private static final String EXCEPTION_MESSAGE = "Responded with {} code. Cause: {}";
  public static final String UNEXPECTED_EXCEPTION = "Unexpected internal error occurred on the server: %s";

  @ResponseStatus(BAD_REQUEST)
  @ExceptionHandler(StatisticsRequestException.class)
  public String handleBadRequests(StatisticsRequestException ex) {
    log.info(EXCEPTION_MESSAGE, BAD_REQUEST.value(), ex.getMessage());
    log.debug("Stacktrace: ", ex);

    return ex.getMessage();
  }

  @ResponseStatus(INTERNAL_SERVER_ERROR)
  @ExceptionHandler(Exception.class)
  public String handleUnexpectedException(Exception ex) {
    String message = String.format(UNEXPECTED_EXCEPTION, ex.getMessage());

    log.info(EXCEPTION_MESSAGE, BAD_REQUEST.value(), message);
    log.info("Stacktrace: ", ex);

    return message;
  }

}
