package com.epam.recommendationservice.controllers;

import com.epam.recommendationservice.domain.CurrencyStatistics;
import com.epam.recommendationservice.dto.CurrencyStatisticsView;
import com.epam.recommendationservice.exceptions.StatisticsRequestException;
import com.epam.recommendationservice.services.CurrencyStatisticsService;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/statistics")
@RequiredArgsConstructor
@Slf4j
public class CurrencyStatisticsController {

  private final CurrencyStatisticsService currencyStatisticsService;
  private final ConversionService conversionService;

  @GetMapping
  List<CurrencyStatisticsView> getAllCurrenciesStats(
      @RequestParam(required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate startDate,
      @RequestParam(required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate endDate) {
    Set<CurrencyStatistics> foundStatistics = currencyStatisticsService.getAllStatistics();

    if (startDate != null || endDate != null) {
      foundStatistics = foundStatistics.stream()
          .map(stats -> currencyStatisticsService.getWithFilteredRecordsByDates(stats, startDate, endDate))
          .filter(Objects::nonNull)
          .collect(Collectors.toSet());
    }

    return foundStatistics.stream()
        .sorted(Comparator.comparing(CurrencyStatistics::getNormalizedRange).reversed())
        .map(statistics -> conversionService.convert(statistics, CurrencyStatisticsView.class))
        .collect(Collectors.toList());
  }

  @GetMapping("/{symbol}")
  CurrencyStatisticsView getCurrencyStats(@PathVariable String symbol,
      @RequestParam(required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate startDate,
      @RequestParam(required = false) @DateTimeFormat(iso = ISO.DATE) LocalDate endDate) {

    CurrencyStatistics foundStatistics = currencyStatisticsService.getStatistics(symbol.toUpperCase());

    if (startDate != null || endDate != null) {
      foundStatistics = currencyStatisticsService.getWithFilteredRecordsByDates(foundStatistics, startDate, endDate);

      if (foundStatistics == null) {
        throw new StatisticsRequestException("No statistics data found within specified range, try to expand it.");
      }
    }

    return conversionService.convert(foundStatistics, CurrencyStatisticsView.class);
  }

  @PatchMapping("/reload")
  Set<String> reload() {
    return currencyStatisticsService.reloadStatistics();
  }
}
