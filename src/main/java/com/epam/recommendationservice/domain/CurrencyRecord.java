package com.epam.recommendationservice.domain;

import com.opencsv.bean.CsvBindByName;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class CurrencyRecord {

  @CsvBindByName(required = true)
  private long timestamp;

  @CsvBindByName(required = true)
  private String symbol;

  @CsvBindByName(required = true)
  private BigDecimal price;

  public boolean isOlderThan(CurrencyRecord other) {
    return this.timestamp < other.getTimestamp();
  }

  public boolean isNewerThan(CurrencyRecord other) {
    return this.timestamp > other.getTimestamp();
  }

  public boolean isCheaperThan(CurrencyRecord other) {
    return this.price.compareTo(other.getPrice()) < 0;
  }

  public boolean isMoreExpensiveThan(CurrencyRecord other) {
    return this.price.compareTo(other.getPrice()) > 0;
  }

  public boolean isWithingDateRange(long startDateTimestamp, long endDateTimestamp) {
    return startDateTimestamp <= timestamp && endDateTimestamp >= timestamp;
  }
}
