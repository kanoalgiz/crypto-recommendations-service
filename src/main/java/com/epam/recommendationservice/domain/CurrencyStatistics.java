package com.epam.recommendationservice.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class CurrencyStatistics {

  private static final int NORMALIZED_RANGE_PRECISION = 5;

  private String symbol;
  private List<CurrencyRecord> allRecords;
  private CurrencyRecord oldestRecord;
  private CurrencyRecord newestRecord;
  private CurrencyRecord mostExpensiveRecord;
  private CurrencyRecord cheapestRecord;
  private BigDecimal normalizedRange;

  public CurrencyStatistics(List<CurrencyRecord> currencyRecords) {
    this.allRecords = currencyRecords;
    this.symbol = currencyRecords.get(0).getSymbol();

    for (CurrencyRecord record : currencyRecords) {
      if (oldestRecord == null || record.isOlderThan(oldestRecord)) {
        oldestRecord = record;
      }

      if (newestRecord == null || record.isNewerThan(newestRecord)) {
        newestRecord = record;
      }

      if (mostExpensiveRecord == null || record.isMoreExpensiveThan(mostExpensiveRecord)) {
        mostExpensiveRecord = record;
      }

      if (cheapestRecord == null || record.isCheaperThan(cheapestRecord)) {
        cheapestRecord = record;
      }
    }

    this.normalizedRange = (mostExpensiveRecord.getPrice().subtract(cheapestRecord.getPrice()))
        .divide(cheapestRecord.getPrice(), NORMALIZED_RANGE_PRECISION, RoundingMode.HALF_UP);
  }
}
