package com.epam.recommendationservice.services;

import com.epam.recommendationservice.dao.CurrencyRecordsDao;
import com.epam.recommendationservice.domain.CurrencyRecord;
import com.epam.recommendationservice.domain.CurrencyStatistics;
import com.epam.recommendationservice.exceptions.StatisticsRequestException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CurrencyStatisticsService {

  private final CurrencyRecordsDao currencyRecordsDao;

  Map<String, CurrencyStatistics> statisticsByCurrency = new HashMap<>();

  public CurrencyStatistics getStatistics(String currencySymbol) {
    if (statisticsByCurrency.containsKey(currencySymbol)) {
      return statisticsByCurrency.get(currencySymbol);
    } else {
      throw new StatisticsRequestException("Requested currency symbol was not found.");
    }
  }

  public Set<CurrencyStatistics> getAllStatistics() {
    return new HashSet<>(statisticsByCurrency.values());
  }

  public CurrencyStatistics getWithFilteredRecordsByDates(CurrencyStatistics statistics, LocalDate startDate, LocalDate endDate) {
    long startDateTimestamp = startDate != null ? startDate.atStartOfDay(ZoneId.of("UTC")).toInstant().toEpochMilli() : 0;
    long endDateTimestamp = endDate != null ? endDate.plusDays(1).atStartOfDay(ZoneId.of("UTC")).toInstant().toEpochMilli() : Long.MAX_VALUE;

    List<CurrencyRecord> filteredRecords = statistics.getAllRecords().stream()
        .filter(record -> record.isWithingDateRange(startDateTimestamp, endDateTimestamp))
        .collect(Collectors.toList());

    if (filteredRecords.isEmpty()) {
      return null;
    }

    return new CurrencyStatistics(filteredRecords);
  }

  @PostConstruct
  public Set<String> reloadStatistics() {
    Set<String> availableCurrencies = currencyRecordsDao.findAvailableCurrencies();
    Map<String, CurrencyStatistics> refreshedStatistics = new HashMap<>();

    for (String currencySymbol : availableCurrencies) {
      List<CurrencyRecord> records = currencyRecordsDao.getCurrencyRecords(currencySymbol);
      refreshedStatistics.put(currencySymbol, new CurrencyStatistics(records));
    }

    statisticsByCurrency = refreshedStatistics;

    log.info("Statistics data reloading is completed.");

    return availableCurrencies;
  }
}
