package com.epam.recommendationservice.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyStatisticsView {

  private String currencySymbol;
  private LocalDate startDate;
  private LocalDate endDate;
  private BigDecimal oldestPrice;
  private BigDecimal newestPrice;
  private BigDecimal maxPrice;
  private BigDecimal minPrice;
  private BigDecimal normalizedRange;
}
