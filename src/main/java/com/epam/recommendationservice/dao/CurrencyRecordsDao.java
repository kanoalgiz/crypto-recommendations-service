package com.epam.recommendationservice.dao;

import com.epam.recommendationservice.domain.CurrencyRecord;
import com.epam.recommendationservice.exceptions.CsvParsingException;
import com.opencsv.bean.CsvToBeanBuilder;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CurrencyRecordsDao {

  private static final String CSV_FILE_EXTENSION = ".csv";
  private static final String STATISTICS_FILE_ENDING = "_values" + CSV_FILE_EXTENSION;
  private static final String RECORD_NOT_VALID_TEMPLATE = "One of statistics records for currency '%s' is not valid and will be skipped: ";

  @Value("${recommendation-service.data-dir}")
  private String dataDirectory;

  public List<CurrencyRecord> getCurrencyRecords(String currencySymbol) {
    try {
      return new CsvToBeanBuilder<CurrencyRecord>(
          new FileReader(dataDirectory + currencySymbol + STATISTICS_FILE_ENDING))
          .withType(CurrencyRecord.class)
          .withIgnoreEmptyLine(true)
          .build()
          .parse()
          .stream()
          .filter(record -> isRecordValid(record, currencySymbol))
          .collect(Collectors.toList());
    } catch (FileNotFoundException ex) {
      throw new CsvParsingException("Csv file was not found for currency " + currencySymbol + ".", ex);
    }
  }

  public Set<String> findAvailableCurrencies() {
    Path path = Paths.get(dataDirectory);

    try (Stream<Path> files = Files.list(path)) {
      return files
          .filter(file -> !Files.isDirectory(file))
          .map(file -> file.getFileName().toString())
          .filter(name -> name.endsWith(STATISTICS_FILE_ENDING))
          .map(name -> name.substring(0, name.indexOf(STATISTICS_FILE_ENDING)).toUpperCase())
          .collect(Collectors.toSet());
    } catch (IOException ex) {
      throw new CsvParsingException("Got an error while searching for csv files in data folder:", ex);
    }
  }

  private boolean isRecordValid(CurrencyRecord record, String expectedCurrencySymbol) {
    if (record.getTimestamp() < 0) {
      log.warn(String.format(RECORD_NOT_VALID_TEMPLATE, expectedCurrencySymbol)
          + "timestamp cannot be negative value (value in the record: '" + record.getTimestamp() + "').");
      return false;
    } else if (!record.getSymbol().equals(expectedCurrencySymbol)) {
      log.warn(String.format(RECORD_NOT_VALID_TEMPLATE, expectedCurrencySymbol)
          + "currency symbol is not valid for given currency (value in the record: '" + record.getSymbol() + "')");
      return false;
    }

    return true;
  }
}
