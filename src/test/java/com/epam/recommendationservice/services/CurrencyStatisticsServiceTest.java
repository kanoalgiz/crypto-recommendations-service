package com.epam.recommendationservice.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.epam.recommendationservice.dao.CurrencyRecordsDao;
import com.epam.recommendationservice.domain.CurrencyRecord;
import com.epam.recommendationservice.domain.CurrencyStatistics;
import com.epam.recommendationservice.exceptions.StatisticsRequestException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CurrencyStatisticsServiceTest {

  @Spy
  @InjectMocks
  private CurrencyStatisticsService currencyStatisticsService;

  @Mock
  CurrencyRecordsDao currencyRecordsDaoMock;

  @Mock
  CurrencyStatistics currencyStatisticsMock;

  @Mock
  CurrencyStatistics secondCurrencyStatisticsMock;

  @BeforeEach
  void beforeEach() {
    currencyStatisticsService.statisticsByCurrency = new HashMap<>();
  }

  @Test
  void getStatistics_currencyNotFound_exceptionIsThrown() {
    StatisticsRequestException exception = assertThrows(StatisticsRequestException.class, () -> currencyStatisticsService.getStatistics("ETH"));

    assertEquals("Requested currency symbol was not found.", exception.getMessage());
  }

  @Test
  void getStatistics_existingCurrency_statisticsObjectReturned() {
    currencyStatisticsService.statisticsByCurrency.put("ETH", currencyStatisticsMock);

    CurrencyStatistics result = currencyStatisticsService.getStatistics("ETH");

    assertEquals(currencyStatisticsMock, result);
  }

  @Test
  void getAllStatistics_returnsAllCachedStatistics() {
    currencyStatisticsService.statisticsByCurrency.put("1", currencyStatisticsMock);
    currencyStatisticsService.statisticsByCurrency.put("2", secondCurrencyStatisticsMock);

    Set<CurrencyStatistics> result = currencyStatisticsService.getAllStatistics();

    assertEquals(2, result.size());
    assertTrue(result.containsAll(Set.of(currencyStatisticsMock, secondCurrencyStatisticsMock)));
  }

  @Test
  void reloadStatistics_twoCurrenciesFound_twoStatisticsCompiled() {
    CurrencyRecord firstRecord = CurrencyRecord.builder()
        .price(new BigDecimal("1.0"))
        .build();
    CurrencyRecord secondRecord = CurrencyRecord.builder()
        .price(new BigDecimal("2.0"))
        .build();
    Mockito.when(currencyRecordsDaoMock.findAvailableCurrencies()).thenReturn(Set.of("AMD", "123"));
    Mockito.when(currencyRecordsDaoMock.getCurrencyRecords("AMD")).thenReturn(List.of(firstRecord));
    Mockito.when(currencyRecordsDaoMock.getCurrencyRecords("123")).thenReturn(List.of(secondRecord));

    Set<String> result = currencyStatisticsService.reloadStatistics();
    Map<String, CurrencyStatistics> resultStatistics = currencyStatisticsService.statisticsByCurrency;

    assertEquals(2, result.size());
    assertTrue(result.containsAll(Set.of("AMD", "123")));
    Mockito.verify(currencyRecordsDaoMock).findAvailableCurrencies();
    Mockito.verify(currencyRecordsDaoMock).getCurrencyRecords("AMD");
    Mockito.verify(currencyRecordsDaoMock).getCurrencyRecords("123");
    assertEquals("1.0", resultStatistics.get("AMD").getCheapestRecord().getPrice().toString());
    assertEquals("2.0", resultStatistics.get("123").getCheapestRecord().getPrice().toString());
    assertEquals("0.00000", resultStatistics.get("AMD").getNormalizedRange().toString());
  }
}