## Recommendation Service

This application is designed to parse files with statistics data for any currencies and can return compiled and easily comparable summaries for each provided currency.

After its startup, application will be available at :8080 port.

### Providing the data

Application expects data to be in csv format with fields 'timestamp', 'symbol', and 'price', for example:

    timestamp,symbol,price
    1641013200000,DOGE,0.1702
    1641074400000,DOGE,0.1722

Each currency data is expected to be in a separate file with name **<CURRENCY_SYMBOL>_values.csv**, for example, **ETH_values.csv** or **USD_values.csv**

You can specify directory for statistic files with **data-dir** property of **application.yml** file.

Some basic validations are performed: any currency file with illegal name will be ignored as well as any files in inner directories.
Within each file, rows with illegal currency symbol will be skipped (So, **BTC_values.csv** is expected to contain only rows with **BTC** symbol).

Application searches and parses csv files at its startup, but you can always add or modify statistic file(s) and manually trigger parsing by calling **PATCH /statistics/reload** endpoint. If reloading was successful, you will receive a response with a list of loaded currencies.

### Accessing compiled statistics

#### Getting specific currency data

To get compiled statistics for one currency type, use **GET /statistics/{CURRENCY_SYMBOL}** endpoint.  (e.g. /statistics/ETH or /statistics/XRP). You will get a JSON response that may look as following:

    {
        "currencySymbol": "ETH",
        "startDate": "2022-01-20",
        "endDate": "2022-01-31",
        "oldestPrice": 3144.78,
        "newestPrice": 2672.5,
        "maxPrice": 3220.04,
        "minPrice": 2336.52,
        "normalizedRange": 0.37814
    }

'startDate' - date of the oldest data record that was used to compile those statistics.  
'endDate' - date of the newest data record.  
All other fields are using data between those two dates.

#### Getting all currencies data

To get data on all available currencies, use **GET /statistics** endpoint. It will return an array of same json objects, as seen above, but for every available currency. Currencies in the list will be sorted by 'normalizedRange' field (descending order) for ease of comparison.

#### Filtering data by date range

If you don't want to use all available data, but you want to get statistics about specific time period - you can use optional url parameters 'startDate' and 'endDate'.

In that case, all data records will be filtered, using given constraints and all response fields will be recalculated using only remaining records.  
Dates are expected to be in ISO DATE format yyyy-MM-dd (e.g. 2022-01-31).  
It is also valid to specify not both startDate and endDate, but only one of them.

Those two parameters are available for both statistic endpoints, so you can request **GET /statistics?startDate=2022-01-20&endDate=2022-02-20** or **GET /statistics/DOGE?startDate=2022-01-20&endDate=2022-02-20**

So, if you want to get statistics of a single specific day - you can just pass equal values for startDate and endDate parameters. In case of /statistics endpoint, all currencies will be still sorted by their normalized range. Currencies that contain zero data records after filtering will not be shown.

